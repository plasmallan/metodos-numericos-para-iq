# Métodos Numéricos para IQ

Del curso de Matlab para Ingenieros Químicos del profesor Ing. Misael González Masías, se muestran los métodos numéricos y problemas en su versión en Python en distintos cuadernos de Jupyter según la lección que se impartió.
